package me.reider45.ExplodingPickaxe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

public class Event implements Listener {

	Main pl;
	public Event(Main in) {
		pl = in;
	}

	public static HashMap<Location, String> locs = new HashMap<Location, String>();
	public static List<Player> immune = new ArrayList<Player>();

	@EventHandler
	public void onBreak(final BlockBreakEvent e)
	{
		if(e.getPlayer().getItemInHand().containsEnchantment(Main.ench))
		{
 
			int level = e.getPlayer().getItemInHand().getEnchantmentLevel(Main.ench);

			int chance = pl.getConfig().getInt("Pickaxe.Level."+level+".Chance");

			Random rand = new Random();
			int random = rand.nextInt(100);

			if( random <= chance )
			{
				
				e.getPlayer().getInventory().addItem(new ItemStack(e.getBlock().getType(), 1));
				locs.put(e.getBlock().getLocation(), e.getPlayer().getName());
				e.getBlock().setType(Material.AIR);
				immune.add(e.getPlayer());

				switch(level)
				{
				case 1:
					e.getPlayer().getWorld().createExplosion(e.getBlock().getLocation(), pl.getConfig().getInt("Pickaxe.Level."+level+".Radius"));
					break;
				case 2:
					e.getPlayer().getWorld().createExplosion(e.getBlock().getLocation(), pl.getConfig().getInt("Pickaxe.Level."+level+".Radius"));
					break;
				case 3:
					e.getPlayer().getWorld().createExplosion(e.getBlock().getLocation(), pl.getConfig().getInt("Pickaxe.Level."+level+".Radius"));
					break;
				}

				Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					public void run()
					{
						locs.remove(e.getBlock().getLocation());
					}
				},10L);

				Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					public void run()
					{
						immune.remove(e.getPlayer());
					}
				},4L);
			}
		}
	}

	@EventHandler
	public void onDMG(EntityDamageEvent e)
	{
		if(e.getCause() == DamageCause.BLOCK_EXPLOSION)
		{
			if(e.getEntity() instanceof Player)
			{
				if(immune.contains( (Player)e.getEntity() ))
				{
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onExplode(EntityExplodeEvent e)
	{
		List<Block> destroyed = e.blockList();
		Iterator<Block> its = destroyed.iterator();
		while (its.hasNext()) 
		{
			Block b = its.next();

			distance(b);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	public void distance(Block block)
	{
		Iterator it = locs.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry<Location, String> pair = (Map.Entry<Location, String>)it.next();

			if(pair.getKey().distance(block.getLocation()) <= 5)
			{
				Bukkit.getPlayer(pair.getValue()).getInventory().addItem(new ItemStack(block.getType(), 1, block.getData()));
			}

		}

	}

}
