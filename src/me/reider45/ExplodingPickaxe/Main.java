package me.reider45.ExplodingPickaxe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements CommandExecutor {


	public static CustomEnchant ench = new CustomEnchant(211);

	public void onEnable()
	{
		saveDefaultConfig();
		try{
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
			Enchantment.registerEnchantment(ench);
		}catch(Exception e){
			e.printStackTrace();
		}
 
		getCommand("ep").setExecutor(this);

		Bukkit.getPluginManager().registerEvents(new Event(this), this);

	}

	public void onDisable()
	{
		Event.locs.clear();
		Event.immune.clear();
	}
	
	public void givePick(Player p, Integer level)
	{
		ItemStack pickaxe = new ItemStack(Material.DIAMOND_PICKAXE, 1);

		ItemMeta pickMeta = pickaxe.getItemMeta();
		pickMeta.setDisplayName(ChatColor.translateAlternateColorCodes('$', getConfig().getString(("Pickaxe.Name"))));
		List<String> lore = new ArrayList<String>();
		lore.add("�7Explosive "+level);
		pickMeta.setLore(lore);
		pickaxe.setItemMeta(pickMeta);

		pickaxe.addUnsafeEnchantment(ench, level);

		p.getInventory().addItem(pickaxe);
	}


	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{

		if(cmd.getName().equalsIgnoreCase("ep"))
		{

			if(args.length == 3)
			{

				if(args[0].equalsIgnoreCase("give"))
				{
					Player p = Bukkit.getPlayer(args[1]);

					if(p != null)
					{

						int level = Integer.parseInt(args[2]);

						if(level == 1 || level == 2 || level == 3)
						{
							givePick(p, level);
						}else{
							sender.sendMessage("Please set the level between 1-3!");
						}

					}else{
						sender.sendMessage("Couldn't find that player!");
					}

				}else{
					sender.sendMessage("/give player level");
				}

			}else{
				sender.sendMessage("/give player level");
			}

		}

		return false;
	}

}
